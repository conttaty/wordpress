<?php
    // Template Name: Home Page
?>

<?php get_header();?>
<main class="titulo">
        <h1><?php the_field('Título chamativo')?>
            </h1>
            <p>
               <?php the_field('Subtitulo chamativo')?></p>
    </main>
    <section class="secao">
        <h2>Uma Mistura de</h2>
        <div class="Dos-itens">
            <div class="itens-secao">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/cafe-1.jpg">
                <h3>amor</h3>
            </div>    
            <div class="itens-secao">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/cafe-2.jpg">
                <h3>perfeição</h3>
            </div>    
        </div>
        <br>
        
        <p>
            O café é uma bebida produzida a partir dos grãos torrados do fruto do cafeeiro. É servido tradicionalmente quente, mas também pode ser consumido gelado. Ele é um estimulante, por possuir cafeína — geralmente 80 a 140 mg para cada 207 ml dependendo do método de preparação. 
        </p>
        <br>
        <br>
        <br>
    </section>
    <section class="secao2">
        <div class="container">
            <div class="bolinhas">
                <div id="bolinhaP"></div>
                    <h2 class="estado">Paulista</h2>
                    <p class="fonte">
                        As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
                    </p>
            </div>
            <div class="bolinhas">
                <div id="bolinhaC"></div>
                    <h2 class="estado">Carioca</h2>
                    <p class="fonte">
                        As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
                    </p>
            </div>   
            <div class="bolinhas">
                    <div id="bolinhaM"></div>
                    <h2 class="estado">Mineira</h2>
                    <p class="fonte">
                        As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
                    </p>
            </div>
        </div>
        <h3 class="botao">SAIBA MAIS</h3>
    </section>
    <section class=secao3>
        <div class="local">
            <div class="Bairros">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/botafogo.jpg">
            </div>    
            <div class="imagem">    
                <h2 class= "algo">Botafogo</h2>
                <p class="algo">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                <h4>VER MAPA</h4>
            </div> 
        </div>  
        <div class="local">     
            <div class="Bairros">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/iguatemi.jpg">
            </div>    
            <div class="imagem">    
                <h2 class="algo">Iguatemi</h2>
                <p class="algo">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                <h4>VER MAPA</h4>
            </div>
        </div>    
        <div class="local"> 
            <div class="Bairros">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/mineirao.jpg">
            </div>    
            <div class="imagem">    
                <h2 class="algo">Mineirão</h2>
                <p class="algo">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                <h4>VER MAPA</h4>
            </div>
        </div>
    </section>
    <section class="assinatura">
        <div class="container">
            <div class="outros">
                <h2>Assine Nossa Newsletter</h2>
                <p>promoções e eventos mensais</p>
            </div>
            <form>
                <label>E-mail</label>
                <input type="text" placeholder="Digite seu e-mail">
                <button type="submit">Enviar</button>
            </form>
        </div>
    </section>
    <?php get_footer();?>