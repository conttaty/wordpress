<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5GzmOciRipAY6CxPExJeb2CoC/zhDAEwzpk2SW0E2gMBMTPTxsqwHjjgaPAy/pSbwpRxrYjOovMI0fpbIsU4/A==');
define('SECURE_AUTH_KEY',  'rHxMcB1q8o97jJCsqhlmTk9S7zSF7WuU2dMlRHl4NA4/lPp2irosQtwcpBT2WYvrp8vA4gpgr8jGMfAtkqbSUA==');
define('LOGGED_IN_KEY',    'Cok29DyNER6saXm+fBrysQAKA6MweTX52ly8s/g/NGZMDx5hFT4HnFYuWyb3/CGaO6hufyPM4mjagGNlRakjUQ==');
define('NONCE_KEY',        'bkorKJ8Cqkvf1KMqeqNmEhKq9oJ9ZoKBdzV+lC04wEWNYAo7EI4Nrm+V0niL21CvFfqLlVtyrLAgf4vvxfNJ7g==');
define('AUTH_SALT',        'rDwzUPuVCzhoUpVqbU9cAPaUWPrhW3xJlhQ9jh48qiBVFSbIiaH/3J9V8fUzGSD4yiR6fsI0cnij/shxjxyLWA==');
define('SECURE_AUTH_SALT', 'mvMU1/vFXQ329Q+QAsNRiZpqxbStwE73wYLY0I3tcjy5EYWdHD/aeyBCP8Mrp2qX/QmcIDry0iW9XRrHbe/y/A==');
define('LOGGED_IN_SALT',   'DuaSGevq9usMSczbGNjYuDhtud6hDhDBSHAmYFiwAr5U0QE/4uMlFQLDPwZ+NKfDqjMY/qFH4sS1Ba2VQo7H/w==');
define('NONCE_SALT',       'TQ0KNtgXgtlRviXPdBq2xaXsrTR4y+Nnwnc6zUmQbaxCc3JrAEi24SXhhTZy+8rlfWPvNNao9+s3uWCZ+R1Rig==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
